//#ifndef SCENE3D_H
#define SCENE3D_H

#include <QtOpenGL/QGLWidget>
#include <QVector> //массив данных динамич.
#include <QFileDialog>
#include <QPixmap>

struct  Model {
    QVector<QVector<GLfloat>> vertexesV2;
    QVector<QVector<GLint>> indexsesV2;
    QVector<QVector<GLfloat>> colorsV2;
    QVector<GLfloat> colors;
    QVector<GLfloat> vertexes;
    QVector<GLfloat> normals;
    QVector<GLint> indexses;
};

class Scene3D : public QGLWidget
{
private:
    // для первой фигуры
    GLfloat xRot1; // угол поворота вокруг оси X
    GLfloat yRot1; // угол поворота вокруг оси Y
    GLfloat zRot1; // угол поворота вокруг оси Z
    GLfloat zTra1; // величина трансляции по оси Z

    // для второй фигуры
    GLfloat xRot2; // угол поворота вокруг оси X
    GLfloat yRot2; // угол поворота вокруг оси Y
    GLfloat zRot2; // угол поворота вокруг оси Z
    GLfloat zTra2; // величина трансляции по оси Z

    QPoint ptrMousePosition;
    GLfloat ratio; // отношение высоты окна виджета к его ширине

//    void scale_plus();
//    void scale_minus();
//    void rotate_up();
//    void rotate_down();
//    void rotate_left();
//    void rotate_right();
//    void translate_down();
//    void translate_up();
//    void defaultScene();
//    void translate_right();
//    void translate_left();

    void drawAxis();
    void drawFigure(Model model);


//    void getVertexArray();
//    void getColorArray();
//    void getIndexArray();

//    void selectFigures(QPoint mp); // выбрать фигуру

//    QTimer *timer; // декларируем таймер


protected:
    void initializeGL();
    void paintGL();
    void resizeGL(int nWidth, int nHeight);
//     void mousePressEvent(QMouseEvent* pe);

//private slots: // слоты

//   //void changeTex(); // поменять текстуры местами
//   void stopTmr(); // остановить таймер
//   void startTmr(); // запустить таймер

public:
    // void drawFigure(Model sputnik);
    Model ReadModelek(QString Itog);
       void change(); // изменение углов поворота и величины трансляции
//    void change(); // изменение углов поворота и величины трансляции
    Scene3D(QWidget* parent = 0);
    Model sputnik;
    Model earth;
//    void mousePressEvent(QMouseEvent* pe);
//    void mouseMoveEvent(QMouseEvent* pe);
//    void mouseReleaseEvent(QMouseEvent* pe);
//    void wheelEvent(QWheelEvent* pe);
//    void keyPressEvent(QKeyEvent* pe);
//    void translate_right();
//    void cngPosition(GLfloat y, GLfloat z);
//    void cngRotation(GLfloat xr, GLfloat yr, GLfloat zr);
//    void cngScale(GLfloat sc);

};
