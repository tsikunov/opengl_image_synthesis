#include <QtGui>
#include <math.h>
#include "scene3D.h"
#include <qdebug.h>
#include <QTimer>
#include <GL/glu.h>

const static float pi=3.141593, k=pi/180;

GLint viewport[4]; // декларируем матрицу поля просмотра

GLfloat VertexArray[12][3];
GLfloat ColorArray[12][3];
GLubyte IndexArray[20][3];

GLint signs[2]={1, 1}; // массив знаков +1 или -1 (понадобятся при трансляции)
bool motionParameters[2]={1, 1}; // массив параметров, определяющих движение

Scene3D::Scene3D(QWidget* parent) : QGLWidget(parent)
{
    xRot1=-90.0f; yRot1=0.0f; zRot1=0.0f; zTra1=0.0f;
    xRot2=-90.0f; yRot2=0.0f; zRot2=0.0f; zTra2=0.0f;

    QString filepass2 = "E:/QtProjects/Partitura/objs/Earth.obj";
    QString filepass1 = "E:/QtProjects/Partitura/objs/Satellite.obj";

    sputnik = ReadModelek(filepass1);
    earth = ReadModelek(filepass2);
    // Model test = sputnik;

    //    timer = new QTimer(this); // создаём таймер
    //    // связываем сигналы и слоты:
    //    connect(timer, SIGNAL(timeout()), this, SLOT(change()));
    //    timer->start(10); // запускаем таймер

    //    // синхронизация кадров с дисплеем:
    //    QGLFormat frmt; // создать формат по умолчанию
    //    frmt.setSwapInterval(1); // установить синхронизацию в формат
    //    setFormat(frmt); // установить формат в контекст
}


Model Scene3D::ReadModelek(QString path)
{
    int times = 0;
    Model result;
    Model neSortirMassiv;
    QFile file(path);
    file.open(QFile::ReadOnly);
    QString str;
    while(!file.atEnd())
    {
        times ++;
        str = file.readLine();
        QStringList sl = str.split(" ");
        if(sl[0] == "#")
        {}
        if(sl[0] == "v")
        {
            neSortirMassiv.vertexes <<sl[1].toFloat()<<sl[2].toFloat()<<sl[3].toFloat();
        }

        if(sl[0] == "vt")
        {}

        if(sl[0] == "vn")
        {
            neSortirMassiv.normals <<sl[1].toFloat()<<sl[2].toFloat()<<sl[3].toFloat();
        }

        if(sl[0] == "f")

        {

            QVector<GLfloat> vert; // промежуточные массивы индексов и точек на одну поверхность
            QVector<GLint> ind;
            for(int i = 1; i < sl.size(); i++ )
            {
                sl[sl.size() - 1] = sl[sl.size() - 1].simplified(); // избавились от n
                QStringList otladka = sl[i].split("/"); // индекс одной вершины
                int vertex = otladka[0].toInt();
                int normal = otladka[2].toInt();
                result.vertexes <<neSortirMassiv.vertexes[(vertex - 1)*3]; // x y z
                result.vertexes <<neSortirMassiv.vertexes[(vertex - 1)*3+1];
                result.vertexes <<neSortirMassiv.vertexes[(vertex - 1)*3+2];

                vert <<neSortirMassiv.vertexes[(vertex - 1)*3]; // x y z
                vert <<neSortirMassiv.vertexes[(vertex - 1)*3+1];
                vert <<neSortirMassiv.vertexes[(vertex - 1)*3+2];

                result.normals <<neSortirMassiv.normals[(normal-1)*3]; // x y z
                result.normals <<neSortirMassiv.normals[(normal-1)*3+1];
                result.normals <<neSortirMassiv.normals[(normal-1)*3+2];

                result.indexses <<i;
                ind<<i;
            }
            result.vertexesV2<<vert; // массив вершин сделан двумерным
            result.indexsesV2<<ind;
            vert.clear();
            ind.clear();

        }

        if(sl[0] == "o")
        {
            //  neSortirMassiv.vertexes.clear();
            // neSortirMassiv.normals.clear();
        }

    }

    file.close();
    result.colorsV2.resize(result.indexsesV2.size());
    for (int i = 0; i < result.colorsV2.size(); i++)
    {
        //float color = 0.1f*(qrand()%11);
        result.colorsV2[i] << 0.3f;//color;//0.1f*(qrand()%11);
        result.colorsV2[i] << 0.3f;//color;//0.1f*(qrand()%11);
        result.colorsV2[i] << 0.3f;//color;//0.1f*(qrand()%11);
    }
    return result;
}

void Scene3D::initializeGL()
{



    qglClearColor(Qt::black);
    glEnable(GL_DEPTH_TEST);
    //    glShadeModel(GL_FLAT);
    //    glEnable(GL_CULL_FACE);



    //    getVertexArray();
    //    getColorArray();
    //    getIndexArray();

    glEnableClientState(GL_VERTEX_ARRAY);
    glEnableClientState(GL_COLOR_ARRAY);
}

void Scene3D::resizeGL(int nWidth, int nHeight)
{
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    GLfloat ratio=(GLfloat)nHeight/(GLfloat)nWidth;

    //    if (width()>=height())
    //       // параметры видимости ортогональной проекции:
    //       glOrtho(-2.0/ratio, 2.0/ratio, -2.0, 2.0, -100.0, 100.0);
    //    else
    //       glOrtho(-2.0, 2.0, -2.0*ratio, 2.0*ratio, -100.0, 100.0);
    glFrustum(-1.0, 1.0, -1.0*ratio, 1.0*ratio, 1.0, 100.0);

    glViewport(0, 0, (GLint)nWidth, (GLint)nHeight);
    //    // извлекаем матрицу поля просмотра в viewport
    glGetIntegerv(GL_VIEWPORT, viewport);
}

void Scene3D::paintGL()
{
    // очищаем буферы изображения и глубины
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // устанавливаем матрицу моделирования текущей
    glMatrixMode(GL_MODELVIEW);
    // загружаем единичную матрицу в матрицу моделирования
    glLoadIdentity();

    float pos[4] = {-1, 5, 0, 1};
    float dir[4] = {0, 0, -100};

    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);

    glLightfv(GL_LIGHT0, GL_POSITION, pos);
    glLightfv(GL_LIGHT0, GL_SPOT_DIRECTION, dir);
    glLightf(GL_LIGHT0, GL_SPOT_CUTOFF, 180.0);
//    glLightf(GL_LIGHT0, GL_SPOT_EXPONENT,10.0);


    // помещаем матрицу моделирования в стек матриц
    glPushMatrix();
    glTranslatef(0.f, -0.0f, -10.0f); // трансляция
    glRotatef(xRot1, 1.0f, 0.0f, 0.0f); // поворот вокруг оси X
    glRotatef(yRot1, 0.0f, 1.0f, 0.0f); // поворот вокруг оси Y
    glRotatef(zRot1, 0.0f, 0.0f, 1.0f); // поворот вокруг оси Z
    // выбираем текстуру для наложения:
    //       glBindTexture(GL_TEXTURE_2D, textureID[textureParameters[0]]);
    drawFigure(sputnik); // нарисовать фигуру
    // извлекаем матрицу моделирования из стека матриц
    glPopMatrix();

    // помещаем матрицу моделирования в стек матриц
    glPushMatrix();
    glTranslatef(25.0f, 2.5f, -40.0f); // трансляция
    glRotatef(xRot2, 1.0f, 0.0f, 0.0f); // поворот вокруг оси X
    glRotatef(yRot2, 0.0f, 1.0f, 0.0f); // поворот вокруг оси Y
    glRotatef(zRot2, 0.0f, 0.0f, 1.0f); // поворот вокруг оси Z
    // выбираем текстуру для наложения:
    //glBindTexture(GL_TEXTURE_2D, textureID[textureParameters[1]]);
    drawFigure(earth); // нарисовать фигуру
    // извлекаем матрицу моделирования из стека матриц
    glPopMatrix();

    glEnable(GL_NORMALIZE);
}

// слот - изменение углов поворота и величины трансляции
void Scene3D::change()
{
   if (motionParameters[0]) // изменение для первой фигуры
   {
      xRot1 -=0.05f;
      yRot1 -=0.05f;
      zRot1 +=0.05f;

      if ((xRot1>360)||(xRot1<-360)) xRot1=0.0f;
      if ((yRot1>360)||(yRot1<-360)) yRot1=0.0f;
      if ((zRot1>360)||(zRot1<-360)) zRot1=0.0f;

      if (abs(zTra1)>0.5f) signs[0] *=-1;
      zTra1 -=signs[0]*0.005f;
   }

   if (motionParameters[1]) // изменение для второй фигуры
   {
      xRot2 +=0.05f;
      yRot2 +=0.05f;
      zRot2 -=0.05f;

      if ((xRot2>360)||(xRot2<-360)) xRot2=0.0f;
      if ((yRot2>360)||(yRot2<-360)) yRot2=0.0f;
      if ((zRot2>360)||(zRot2<-360)) zRot2=0.0f;

      if (abs(zTra2)>0.5f) signs[1] *=-1;
      zTra2 +=signs[1]*0.005f;
   }

   updateGL(); // обновить изображение
}

//void Scene3D::stopTmr() // слот - остановить таймер
//{
//   timer->stop();
//}

//void Scene3D::startTmr() // слот - запустить таймер
//{
//   timer->start();
//}

//void Scene3D::mousePressEvent(QMouseEvent* pe)
//{
//    ptrMousePosition = pe->pos();
//}

//void Scene3D::mouseReleaseEvent(QMouseEvent* pe)
//{

//}


//void Scene3D::mouseMoveEvent(QMouseEvent* pe)
//{
//    xRot += 180/nSca*(GLfloat)(pe->y()-ptrMousePosition.y())/height();
//    zRot += 180/nSca*(GLfloat)(pe->x()-ptrMousePosition.x())/width();

//    ptrMousePosition = pe->pos();

//    updateGL();
//}

//void Scene3D::wheelEvent(QWheelEvent* pe)
//{
//    if ((pe->delta())>0) scale_plus(); else if ((pe->delta())<0) scale_minus();

//    updateGL();
//}

//void Scene3D::keyPressEvent(QKeyEvent* pe)
//{
//    switch (pe->key())
//    {
//    case Qt::Key_Plus:
//        scale_plus();
//        break;

//    case Qt::Key_Equal:
//        scale_plus();
//        break;

//    case Qt::Key_Minus:
//        scale_minus();
//        break;

//    case Qt::Key_I:
//        rotate_up();
//        break;

//    case Qt::Key_K:
//        rotate_down();
//        break;

//    case Qt::Key_J:
//        rotate_left();
//        break;

//    case Qt::Key_L:
//        rotate_right();
//        break;

//    case Qt::Key_Z:
//        translate_down();
//        break;

//    case Qt::Key_X:
//        translate_up();
//        break;

//    case Qt::Key_C:
//        translate_right();
//        break;

//    case Qt::Key_V:
//        translate_left();
//        break;

//    case Qt::Key_Space:
//        defaultScene();
//        break;

//    case Qt::Key_Escape:
//        this->close();
//        break;
//    }

//    updateGL();
//}

//void Scene3D::scale_plus()
//{
//    nSca = nSca*1.1;
//}

//void Scene3D::scale_minus()
//{
//    nSca = nSca/1.1;
//}

//void Scene3D::rotate_up()
//{
//    xRot += 1.0;
//}

//void Scene3D::rotate_down()
//{
//    xRot -= 1.0;
//}

//void Scene3D::rotate_left()
//{
//    zRot += 1.0;
//}

//void Scene3D::rotate_right()
//{
//    zRot -= 1.0;
//}

//void Scene3D::translate_down()
//{
//    zTra -= 0.05;
//}

//void Scene3D::translate_up()
//{
//    zTra += 0.05;
//}

//void Scene3D::translate_right()
//{
//    yTra -= 0.05;
//}

//void Scene3D::translate_left()
//{
//    yTra += 0.05;
//}

//void Scene3D::defaultScene()
//{
//    xRot=-90; yRot=0; zRot=0; zTra=0; nSca=1; yTra = 0;
//}



void Scene3D::drawFigure(Model model)
{
    for(int j=0; j < model.indexsesV2.size(); j++)
    {
        glColor3d(model.colorsV2[j][0] , model.colorsV2[j][1], model.colorsV2[j][2]);
        glBegin(GL_POLYGON);
        for(int i = 0; i < model.indexsesV2[j].size(); i++)
        {
            glVertex3d( model.vertexesV2[j][i*3], model.vertexesV2[j][i*3+1], model.vertexesV2[j][i*3+2]);
        }
        glEnd();
    }
}


//void Scene3D::cngPosition(GLfloat y, GLfloat z){
//    yTra = y;
//    zTra = z;
//}

//void Scene3D::cngRotation(GLfloat xr, GLfloat yr, GLfloat zr){
//    xRot = xr;
//    yRot = yr;
//    zRot = zr;
//}

//void Scene3D::cngScale(GLfloat sc){
//    nSca = sc;
//}
